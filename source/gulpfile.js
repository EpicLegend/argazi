'use strict';

var gulp 	= require('gulp'),
    sass 	= require('gulp-sass'),
   	watch 	= require('gulp-watch'),
   	pug 	= require('gulp-pug'),
   	rename  = require('gulp-rename');

gulp.task('sass', function () {
	console.log("complete sass");
   return gulp.src('sass/style.scss')
   		  .pipe(sass())
          .pipe(gulp.dest('../build/css/'));
});

gulp.task('pug', function () {
	console.log("complete pug");
	return gulp.src('html/main.pug')
		   .pipe(pug({pretty: true}))
		   .pipe(rename('index.html'))
		   .pipe(gulp.dest("../build"));
});


gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/main.pug', gulp.series('pug'));
});


gulp.task('default', gulp.series(['sass', 'pug'], 'watch'));